*docker-compose up --build*


1) GET http://127.0.0.1:5000/boxes/ - Get list of boxes
2) POST http://127.0.0.1:5000/boxes/ - Create box
3) GET http://127.0.0.1:5000/boxes/<name> - Get box
4) POST http://127.0.0.1:5000/boxes/T-Sh - Create item of box
5) GET http://127.0.0.1:5000/boxes/T-Sh - Get items of box
