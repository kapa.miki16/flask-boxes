import os

DATABASE_URL = os.environ.get('DATABASE_URL', 'postgresql://postgres:postgres@localhost/postgres')
ALLOWED_COLORS = ['red', 'green', 'blue', 'yellow', 'magenta']
