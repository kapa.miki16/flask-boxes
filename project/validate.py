from exception import InvalidAPIUsage
from settings import ALLOWED_COLORS

from models import Box


def is_exists_box(name):
    return bool(Box.query.filter_by(name=name).first())


def validate_box_exists(name):
    if is_exists_box(name):
        raise InvalidAPIUsage(f'Box by name {name} already exists', status_code=400)


def validate_color_allowed(color):
    if color.lower() not in ALLOWED_COLORS:
        raise InvalidAPIUsage('Not allowed color', status_code=400)


def validate_create_box(data):
    try:
        name = data['name']
        color = data['color']
    except KeyError:
        raise InvalidAPIUsage('Invalid data', status_code=400)

    validate_color_allowed(color)
    validate_box_exists(name)


def get_box_or_404(name):
    box = Box.query.filter_by(name=name).first()
    if not box:
        raise InvalidAPIUsage('Box not found', status_code=404)
    return box


def validate_create_box_item(data):
    try:
        name = data['name']
        return name
    except KeyError:
        raise InvalidAPIUsage('Invalid data', status_code=400)
