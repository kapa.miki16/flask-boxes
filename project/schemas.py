from flask_marshmallow import Marshmallow
from flask_marshmallow.sqla import SQLAlchemySchema

from models import Box, BoxItem

ma = Marshmallow()


class BoxSchema(SQLAlchemySchema):
    class Meta:
        model = Box

    id = ma.auto_field()
    name = ma.auto_field()
    color = ma.auto_field()


class BoxItemSchema(SQLAlchemySchema):
    class Meta:
        model = BoxItem

    id = ma.auto_field()
    name = ma.auto_field()
    box_id = ma.auto_field()
