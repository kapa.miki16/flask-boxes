from database import db


class Box(db.Model):
    id = db.Column(
        db.Integer,
        primary_key=True
    )
    name = db.Column(
        db.String(255),
        unique=True,
        nullable=False
    )
    color = db.Column(
        db.String(255),
        nullable=False
    )
    items = db.relationship(
        'BoxItem',
        backref='box',
        lazy=True
    )

    def __init__(self, name, color):
        self.name = name
        self.color = color

    def create(self):
        db.session.add(self)
        db.session.commit()
        return self


class BoxItem(db.Model):
    id = db.Column(
        db.Integer,
        primary_key=True
    )
    name = db.Column(
        db.String(255),
        nullable=False
    )
    box_id = db.Column(
        db.Integer,
        db.ForeignKey('box.id'),
        nullable=False
    )

    def __init__(self, name, box_id):
        self.name = name
        self.box_id = box_id

    def create(self):
        db.session.add(self)
        db.session.commit()
        return self
