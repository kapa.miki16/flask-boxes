from collections import Counter

from flask.blueprints import Blueprint
from flask import request, jsonify

from schemas import BoxSchema, BoxItemSchema
from models import Box, BoxItem
from validate import validate_create_box, get_box_or_404, validate_create_box_item


boxes = Blueprint(
    'boxes',
    __name__,
)


@boxes.route('/', methods=['GET'])
def get_boxes():
    boxes = Box.query.all()
    schema = BoxSchema(many=True)
    return schema.jsonify(boxes)


@boxes.route('/', methods=['POST'])
def create_box():
    data = request.get_json()

    validate_create_box(data)

    box = Box(name=data['name'], color=data['color'])
    box.create()
    schema = BoxSchema()
    return schema.jsonify(box)


@boxes.route('/<name>/', methods=['POST'])
def create_item(name):
    data = request.get_json()
    validate_create_box_item(data)

    box = get_box_or_404(name)
    item = BoxItem(name=data['name'], box_id=box.id)
    item.create()
    schema = BoxItemSchema()
    return schema.jsonify(item)


@boxes.route('/<name>/', methods=['GET'])
def get_items(name):
    box = get_box_or_404(name)
    items = box.items
    schema = BoxItemSchema(many=True)
    data = schema.dump(items)
    count_of_names = Counter(item['name'] for item in data)

    for item in data:
        item['name'] = f"{item['name']} ({count_of_names[item['name']]})"
    return jsonify(data)
