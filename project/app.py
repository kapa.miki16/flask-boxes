from flask import Flask, jsonify
from flask_migrate import Migrate

from settings import DATABASE_URL
from database import db
from views import boxes
from schemas import ma
from exception import InvalidAPIUsage
from models import Box, BoxItem


def create_app():
    app = Flask(__name__)
    app.config['DEBUG'] = True
    app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URL

    db.init_app(app)
    migrate = Migrate(app, db)

    ma.init_app(app)

    app.register_blueprint(boxes, url_prefix='/boxes/')

    @app.errorhandler(InvalidAPIUsage)
    def invalid_api_usage(e):
        return jsonify(e.to_dict()), e.status_code

    return app


if __name__ == '__main__':
    app = create_app()
    app.run()
